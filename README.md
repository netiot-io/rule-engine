# Rule-engine
-------------------------
Receives aggregated data and processes it using drools. Receives data on kafka from the app-processing service. 
Can send kafka messages in response to the rules.
 
## Known paths
```
POST http://localhost:8081/V1/command-rules/internal/{userId}/{applicationId} \\ adds rule list to applicationId
```
```
BODY
[
	{
		"destinationDeviceId": 40,
		"ruleName": "Test",
		"rule": "import io.netiot.ruleengine.models.DataContainer;\n\nrule \"Test\"\n\twhen\n\t\troomData : DataContainer()\n\t\teval((Integer) roomData.getInputProperty(11L, \"Occupancy\", \"value\") == 0)\n\tthen\n\t\tDouble Tmax = 25.5;\n\t\troomData.setOutputProperty(\"SetPoint\", \"value\", Tmax);\nend\n\nrule \"Test2\"\n\twhen\n\t\troomData : DataContainer()\n\t\teval((Integer) roomData.getInputProperty(11L, \"Occupancy\", \"value\") != 0)\n\tthen\n\t\tDouble Tmax = 25.5;\n\t\tDouble Tmin = 12.5;\n\t\tDouble Teq = 21.0;\n\t\tDouble alpha = 0.5;\n\t\tDouble setTemp = (Double) roomData.getInputProperty(10L, \"SetPoint\", \"value\") - alpha * ((Double) roomData.getInputProperty(10L, \"RoomTemperature\", \"value\") - Teq);\n\t\tsetTemp = setTemp > Tmin ? setTemp : Tmin;\n\t\tsetTemp = setTemp < Tmax ? setTemp : Tmax;\n\t\troomData.setOutputProperty(\"SetPoint\", \"value\", setTemp);\nend"
	}
]
```
-------------------------
```
GET http://localhost:8081/V1/command-rules/internal/{applicationId} \\ gets the rule list from applicationId
```

```
RESPONSE
[
	{
		"destinationDeviceId": 40,
		"ruleName": "Test",
		"rule": "import io.netiot.ruleengine.models.DataContainer;\n\nrule \"Test\"\n\twhen\n\t\troomData : DataContainer()\n\t\teval((Integer) roomData.getInputProperty(11L, \"Occupancy\", \"value\") == 0)\n\tthen\n\t\tDouble Tmax = 25.5;\n\t\troomData.setOutputProperty(\"SetPoint\", \"value\", Tmax);\nend\n\nrule \"Test2\"\n\twhen\n\t\troomData : DataContainer()\n\t\teval((Integer) roomData.getInputProperty(11L, \"Occupancy\", \"value\") != 0)\n\tthen\n\t\tDouble Tmax = 25.5;\n\t\tDouble Tmin = 12.5;\n\t\tDouble Teq = 21.0;\n\t\tDouble alpha = 0.5;\n\t\tDouble setTemp = (Double) roomData.getInputProperty(10L, \"SetPoint\", \"value\") - alpha * ((Double) roomData.getInputProperty(10L, \"RoomTemperature\", \"value\") - Teq);\n\t\tsetTemp = setTemp > Tmin ? setTemp : Tmin;\n\t\tsetTemp = setTemp < Tmax ? setTemp : Tmax;\n\t\troomData.setOutputProperty(\"SetPoint\", \"value\", setTemp);\nend"
	}
]
```

-------------------------
```
DELETE http://localhost:8081/V1/command-rules/internal/{applicationId} \\ deletes the rule list from applicationId
```
-------------------------
```
GET http://localhost:8081/V1/engine-alerts/internal/all/{applicationId} \\gets all the alert messages
```
```
RESPONSE
[
    {
        "id": 7,
        "applicationId": 29,
        "deviceName": "test",
        "code": "backend.rules.engine.command.success",
        "success": true,
        "timestamp": "2019-09-03T10:36:00.103445",
        "errorMessage": null,
        "sensors": [
            "externalTemperature"
        ]
    },
    {
        "id": 6,
        "applicationId": 29,
        "deviceName": "test",
        "code": "backend.rules.engine.execution.failed",
        "success": false,
        "timestamp": "2019-09-03T09:50:52.759222",
        "errorMessage": "defaultpkg.Rule_Test1109811651Eval0Invoker@2bb541ac : java.lang.NullPointerException",
        "sensors": null
    }
]
```
-------------------------
```
GET http://localhost:8081/V1/engine-alerts/internal/last10/{applicationId} \\gets last 10 the alert messages
```
```
RESPONSE
[
    {
        "id": 7,
        "applicationId": 29,
        "deviceName": "test",
        "code": "backend.rules.engine.command.success",
        "success": true,
        "timestamp": "2019-09-03T10:36:00.103445",
        "errorMessage": null,
        "sensors": [
            "externalTemperature"
        ]
    },
    {
        "id": 6,
        "applicationId": 29,
        "deviceName": "test",
        "code": "backend.rules.engine.execution.failed",
        "success": false,
        "timestamp": "2019-09-03T09:50:52.759222",
        "errorMessage": "defaultpkg.Rule_Test1109811651Eval0Invoker@2bb541ac : java.lang.NullPointerException",
        "sensors": null
    }...
]
```

## DB
Database migration implemented with [flyway](https://flywaydb.org/)
Scrips in `resources/db/migration`.

## Feign
Communicates with the `devices` microservice to get the device name and the topic based on the `deviceId`.

`GET devices/V1/devices/internal/{deviceId}/topic`

Return: `string`

`GET devices/V1/devices/internal/{deviceId}`

Return: `string`

## Kafka
Received messages from Kafka like:
```
{"appId":29,"latestDeviceData":{"28":{"externalTemperature":{"value":21.7}}}}
```
And emits multiple events by executing the rules associated with the `appId`. The events are commands for devices like:
```
{"deviceId":28,"timestamp":1567069434455,"data":[{"externalTemperature":{"value":21.35}}]}
```
or can be alerts that are saved to the DB.

## CI/CD
Compile, test, package, build docker image and push to gitlab docker registry.

## Tests
N/A