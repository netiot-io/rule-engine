FROM anapsix/alpine-java:8
ADD target/rule-engine.jar rule-engine.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /rule-engine.jar
