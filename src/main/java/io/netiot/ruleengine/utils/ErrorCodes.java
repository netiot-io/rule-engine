package io.netiot.ruleengine.utils;

public final class ErrorCodes {
    private ErrorCodes() {}

    public static final String RULE_COMPILATION_FAILED_ERROR_CODE = "backend.rules.engine.compilation.failed";
    public static final String RULE_EXECUTION_FAILED_ERROR_CODE = "backend.rules.engine.execution.failed";
    public static final String COMMAND_NOT_SENT_ERROR_CODE = "backend.rules.engine.command.not.sent";
    public static final String COMMAND_SUCCESS_ERROR_CODE = "backend.rules.engine.command.success";
}
