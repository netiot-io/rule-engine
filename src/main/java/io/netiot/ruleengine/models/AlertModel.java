package io.netiot.ruleengine.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class AlertModel {
    private Long id;
    private Long applicationId;
    private String deviceName;
    private String code;
    private Boolean success;
    private LocalDateTime timestamp;
    private String errorMessage;
    private List<String> sensors;
}
