package io.netiot.ruleengine.models;

import lombok.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataContainer {
    @Setter(AccessLevel.PRIVATE)
    private Long applicationId;
    private Map<Long, Map<String, Map<String, Object>>> inputData;
    private Map<String, Map<String, Object>> outputData;

    public Object getInputProperty(Long deviceId, String sensorName, String property)
    {
        return inputData.get(deviceId).get(sensorName).get(property);
    }

    public void setOutputProperty(String name, String property, Object value)
    {
        Map<String, Object> properties;

        if (outputData.containsKey(name)) {
            properties = outputData.get(name);
        } else {
            properties = new HashMap<>();
            outputData.put(name, properties);
        }

        properties.put(property, value);
    }

    public static DataContainer fromEvent(EventData sample) {
        Map<Long, Map<String, Map<String, Object>>> inputData = sample.getLatestDeviceData();

        return new DataContainerBuilder()
                .inputData(inputData)
                .applicationId(sample.getAppId())
                .outputData(new HashMap<>())
                .build();
    }

    public static CommandData toCommand(DataContainer container) {
        List<Map<String, Map<String, Object>>> commandData = container.outputData
                .entrySet()
                .stream()
                .map(x -> {
                    Map<String, Map<String, Object>> properties = new HashMap<>();
                    properties.put(x.getKey(), x.getValue());

                    return properties;
                })
                .collect(Collectors.toList());

        return new CommandData.CommandDataBuilder()
                .data(commandData)
                .timestamp(System.currentTimeMillis())
                .applicationId(container.applicationId)
                .build();
    }
}
