package io.netiot.ruleengine.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommandData {
    @JsonIgnore
    private Long applicationId;
    private Long deviceId;
    private Long timestamp;
    private List<Map<String, Map<String, Object>>> data;

    @JsonIgnore
    private String destinationTopic;
}
