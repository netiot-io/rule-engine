package io.netiot.ruleengine.repositories;

import io.netiot.ruleengine.entities.EngineAlert;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EngineAlertsRepository extends JpaRepository<EngineAlert, Long> {
    List<EngineAlert> findByApplicationIdOrderByTimestampDesc(Long applicationId);
    List<EngineAlert> findTop10ByApplicationIdOrderByTimestampDesc(Long applicationId);
}
