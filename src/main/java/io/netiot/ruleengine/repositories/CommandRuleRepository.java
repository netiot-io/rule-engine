package io.netiot.ruleengine.repositories;

import io.netiot.ruleengine.entities.CommandRuleList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CommandRuleRepository extends JpaRepository<CommandRuleList, Long> {
    Optional<CommandRuleList> findByApplicationId(Long applicationId);
    void deleteByApplicationId(Long applicationId);
}
