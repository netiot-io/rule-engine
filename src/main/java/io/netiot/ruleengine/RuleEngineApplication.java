package io.netiot.ruleengine;

import io.netiot.ruleengine.configuration.KafkaChannels;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(KafkaChannels.class)
@SpringBootApplication
public class RuleEngineApplication {
	public static void main(String[] args) {
		SpringApplication.run(RuleEngineApplication.class, args);
	}
}
