package io.netiot.ruleengine.processors;

import io.netiot.ruleengine.models.CommandData;
import io.netiot.ruleengine.models.EventData;
import io.netiot.ruleengine.services.RuleProcessingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.kstream.*;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;


import static io.netiot.ruleengine.configuration.KafkaBindings.DATA_IN;
import static io.netiot.ruleengine.configuration.KafkaBindings.DATA_OUT;

@Slf4j
@Component
@RequiredArgsConstructor
public class DataEventProcessor {
    private final RuleProcessingService ruleProcessingService;

    @StreamListener
    @SendTo(DATA_OUT)
    public KStream<String, CommandData> process(@Input(DATA_IN) KStream<String, EventData> events) {
        KStream<String, CommandData> processor = events
                .peek((k, v) -> log.info("Received object: " + v.toString()))
                .flatMapValues(ruleProcessingService::processEvent)
                .peek((k, v) -> log.info("Sending object: " + v.toString()));
        processor.to((key, commandData, recordContext) -> commandData.getDestinationTopic());

        return processor;
    }
}
