package io.netiot.ruleengine.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netiot.ruleengine.models.CommandData;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;

@Converter(autoApply = true)
public class CommandDataConverter implements AttributeConverter<CommandData, String> {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(CommandData entity) {
        try {
            return objectMapper.writeValueAsString(entity);
        } catch (JsonProcessingException ex) {
            return null;
        }
    }

    @Override
    public CommandData convertToEntityAttribute(String entity) {
        try {
            return objectMapper.readValue(entity, CommandData.class);
        } catch (IOException ex) {
            return null;
        }
    }
}