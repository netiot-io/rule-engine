package io.netiot.ruleengine.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netiot.ruleengine.entities.CommandRule;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Converter(autoApply = true)
public class CommandRuleListConverter implements AttributeConverter<List<CommandRule>, String> {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(List<CommandRule> entity) {
        try {
            return objectMapper.writeValueAsString(entity);
        } catch (JsonProcessingException ex) {
            return null;
        }
    }

    @Override
    public List<CommandRule> convertToEntityAttribute(String entity) {
        try {
            return objectMapper.readValue(entity, new TypeReference<List<CommandRule>>(){});
        } catch (IOException ex) {
            return new ArrayList<>();
        }
    }
}