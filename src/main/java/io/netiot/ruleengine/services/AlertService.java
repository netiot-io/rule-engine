package io.netiot.ruleengine.services;

import io.netiot.ruleengine.entities.EngineAlert;
import io.netiot.ruleengine.models.AlertModel;
import io.netiot.ruleengine.models.CommandData;
import io.netiot.ruleengine.repositories.EngineAlertsRepository;
import io.netiot.ruleengine.utils.ErrorCodes;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AlertService {
    private final EngineAlertsRepository alertsRepository;
    private final DeviceService deviceService;

    public void handleError(Long applicationId, Long destinationDeviceId, String errorCode, String errorMessage) {
        log.error("An error occurred on application data with ID " + applicationId +
                " " + errorCode + " " + errorMessage);

        EngineAlert alert = EngineAlert.builder()
                .applicationId(applicationId)
                .destinationDeviceId(destinationDeviceId)
                .code(errorCode)
                .errorMessage(errorMessage)
                .timestamp(LocalDateTime.now())
                .build();

        alertsRepository.save(alert);
    }

    public void handleEvent(Long applicationId, CommandData outputData) {
        EngineAlert alert = EngineAlert.builder()
                .applicationId(applicationId)
                .code(ErrorCodes.COMMAND_SUCCESS_ERROR_CODE)
                .destinationDeviceId(outputData.getDeviceId())
                .outputData(outputData)
                .timestamp(LocalDateTime.now())
                .build();

        alertsRepository.save(alert);
    }

    public List<AlertModel> getAllAlerts(Long applicationId)
    {
        return alertListToModelList(alertsRepository.findByApplicationIdOrderByTimestampDesc(applicationId));
    }

    public List<AlertModel> getLast10Alerts(Long applicationId)
    {
        return alertListToModelList(alertsRepository.findTop10ByApplicationIdOrderByTimestampDesc(applicationId));
    }

    public List<AlertModel> alertListToModelList(List<EngineAlert> engineAlerts)
    {
        return engineAlerts.stream()
                .map(this::alertToModel)
                .collect(Collectors.toList());
    }

    public AlertModel alertToModel(EngineAlert engineAlert)
    {
        return AlertModel.builder()
                .id(engineAlert.getId())
                .applicationId(engineAlert.getApplicationId())
                .success(engineAlert.getCode().equals(ErrorCodes.COMMAND_SUCCESS_ERROR_CODE))
                .code(engineAlert.getCode())
                .deviceName(deviceService.getDeviceName(engineAlert.getDestinationDeviceId()))
                .errorMessage(engineAlert.getErrorMessage())
                .timestamp(engineAlert.getTimestamp())
                .sensors(engineAlert.getOutputData() == null ? null :
                        engineAlert.getOutputData().getData()
                                .stream()
                                .flatMap(x -> x.keySet().stream()).collect(Collectors.toList()))
                .build();
    }
}
