package io.netiot.ruleengine.services;

import io.netiot.ruleengine.entities.CommandRule;
import io.netiot.ruleengine.entities.CommandRuleList;
import io.netiot.ruleengine.models.*;
import io.netiot.ruleengine.repositories.CommandRuleRepository;
import io.netiot.ruleengine.utils.ErrorCodes;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.drools.core.impl.InternalKnowledgeBase;
import org.drools.core.impl.KnowledgeBaseFactory;
import org.kie.api.definition.KiePackage;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderError;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.StringReader;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class RuleProcessingService {
    private final CommandRuleRepository ruleRepository;
    private final AlertService alertService;
    private final DeviceService deviceService;

    public List<CommandRule> getApplicationRules(Long applicationId) {
        return ruleRepository
                .findByApplicationId(applicationId)
                .map(CommandRuleList::getRules)
                .orElse(new ArrayList<>());
    }

    public String getDeviceTopic(CommandData command) {
        try {
            return deviceService.getDeviceTopic(command.getDeviceId());
        } catch (Exception ex) {
            alertService.handleError(command.getApplicationId(),
                    command.getDeviceId(),
                    ErrorCodes.COMMAND_NOT_SENT_ERROR_CODE,
                    "Could not retrieve topic of device with ID: " + command.getDeviceId());
        }

        return null;
    }

    private DataContainer applyRule(Long applicationId, DataContainer dataContainer, CommandRule rule) {
        Resource ruleResource = ResourceFactory.newReaderResource(new StringReader(rule.getRule()));
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(ruleResource, ResourceType.DRL);

        if (kbuilder.hasErrors()) {
            alertService.handleError(applicationId,
                    rule.getDestinationDeviceId(),
                    ErrorCodes.RULE_COMPILATION_FAILED_ERROR_CODE,
                    kbuilder.getErrors()
                            .stream()
                            .map(KnowledgeBuilderError::getMessage)
                            .reduce((a, b) -> a + "\n" + b)
                            .orElse("")
            );

            return null;
        }

        Collection<KiePackage> pkgs = kbuilder.getKnowledgePackages();
        InternalKnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
        kbase.addPackages(pkgs);
        KieSession ksession = kbase.newKieSession();
        ksession.insert(dataContainer);

        try {
            ksession.fireAllRules();
        } catch (Exception ex) {
            alertService.handleError(applicationId,
                    rule.getDestinationDeviceId(),
                    ErrorCodes.RULE_EXECUTION_FAILED_ERROR_CODE, ex.getMessage());

            return null;
        } finally {
            ksession.destroy();
        }

        return dataContainer;
    }

    public List<CommandData> processEvent(EventData event) {
        setNumericsToDouble(event);
        List<CommandRule> rules = getApplicationRules(event.getAppId());

        return rules.stream()
                .map(
                        rule -> {
                            DataContainer dataContainer = applyRule(event.getAppId(),
                                                                    DataContainer.fromEvent(event),
                                                                    rule);

                            if (dataContainer == null)
                                return null;

                            CommandData command = DataContainer.toCommand(dataContainer);
                            command.setDeviceId(rule.getDestinationDeviceId());

                            return command;
                        }
                )
                .filter(Objects::nonNull)
                .peek(commandData -> commandData.setDestinationTopic(this.getDeviceTopic(commandData)))
                .filter(commandData -> commandData.getDestinationTopic() != null)
                .peek(commandData -> {
                    alertService.handleEvent(event.getAppId(), commandData);
                })
                .collect(Collectors.toList());
    }


    public void addCommandRuleList(Long userId,
                                   Long applicationId,
                                   List<CommandRule> rules) {
        Optional<CommandRuleList> commandRuleList = ruleRepository
                                                    .findByApplicationId(applicationId);

        if (commandRuleList.isPresent()) {
            commandRuleList.get().setUserId(userId);
            commandRuleList.get().setRules(rules);

            return;
        }

        CommandRuleList newCommandRuleList = CommandRuleList.builder()
                .userId(userId)
                .applicationId(applicationId)
                .rules(rules)
                .build();

        ruleRepository.saveAndFlush(newCommandRuleList);
    }

    private static void setNumericsToDouble(EventData event)
    {
        event.getLatestDeviceData()
                .forEach((k, v) -> {
                    v.forEach((k2, v2) -> {
                        Set<String> keys = v2.keySet();

                        for (String key : keys) {
                            if (v2.get(key) instanceof Integer)
                                v2.put(key, Double.valueOf((Integer) (v2.get(key))));
                            else if (v2.get(key) instanceof Long)
                                v2.put(key, Double.valueOf((Long) (v2.get(key))));
                        }
                    });
                });
    }

    public void deleteCommandRules(Long applicationId) {
        ruleRepository.deleteByApplicationId(applicationId);
    }
}
