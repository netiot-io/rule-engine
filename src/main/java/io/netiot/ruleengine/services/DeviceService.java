package io.netiot.ruleengine.services;

import io.netiot.ruleengine.feign.DeviceClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DeviceService {
    private final DeviceClient deviceClient;

    public String getDeviceTopic(Long deviceId) {
        return deviceClient.getDeviceTopic(deviceId);
    }
    public String getDeviceName(Long deviceId) {
        return deviceClient.getDeviceName(deviceId);
    }
}
