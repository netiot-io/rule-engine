package io.netiot.ruleengine.entities;

import io.netiot.ruleengine.converters.CommandRuleListConverter;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@Table(name = "COMMAND_RULES")
@NoArgsConstructor
@AllArgsConstructor
public class CommandRuleList {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_COMMAND_RULES_GEN")
    @SequenceGenerator(name = "SEQ_COMMAND_RULES_GEN", sequenceName = "SEQ_COMMAND_RULES", allocationSize = 1)
    Long id;

    @Column(name = "user_id")
    Long userId;

    @Column(name = "application_id")
    Long applicationId;

    @Convert(converter = CommandRuleListConverter.class)
    List<CommandRule> rules;
}
