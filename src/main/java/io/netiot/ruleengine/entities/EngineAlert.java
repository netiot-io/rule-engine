package io.netiot.ruleengine.entities;

import io.netiot.ruleengine.converters.CommandDataConverter;
import io.netiot.ruleengine.models.CommandData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "ENGINE_ALERTS")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class EngineAlert {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ENGINE_ALERTS_GEN")
    @SequenceGenerator(name = "SEQ_ENGINE_ALERTS_GEN", sequenceName = "SEQ_ENGINE_ALERTS", allocationSize = 1)
    private Long id;

    @Column(name = "application_id")
    private Long applicationId;

    @Column(name = "destination_device_id")
    private Long destinationDeviceId;
    private String code;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime timestamp;

    @Column(name = "error_message")
    private String errorMessage;

    @Convert(converter = CommandDataConverter.class)
    @Column(name = "output_data")
    private CommandData outputData;
}
