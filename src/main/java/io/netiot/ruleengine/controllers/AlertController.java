package io.netiot.ruleengine.controllers;

import io.netiot.ruleengine.services.AlertService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/engine-alerts")
public class AlertController {
    private final AlertService alertService;

    @GetMapping("/internal/all/{applicationId}")
    public ResponseEntity getAllAlerts(@PathVariable(name = "applicationId") final Long applicationId) {
        return ResponseEntity.ok(alertService.getAllAlerts(applicationId));
    }

    @GetMapping("/internal/last10/{applicationId}")
    public ResponseEntity getLast10Alerts(@PathVariable(name = "applicationId") final Long applicationId) {
        return ResponseEntity.ok(alertService.getLast10Alerts(applicationId));
    }
}
