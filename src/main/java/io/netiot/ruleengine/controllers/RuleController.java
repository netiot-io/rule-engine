package io.netiot.ruleengine.controllers;

import io.netiot.ruleengine.entities.CommandRule;
import io.netiot.ruleengine.services.RuleProcessingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/command-rules")
public class RuleController {
    private final RuleProcessingService ruleProcessingService;

    @PostMapping("/internal/{userId}/{applicationId}")
    public ResponseEntity addRules(@PathVariable(name = "userId") final Long userId,
                                         @PathVariable(name = "applicationId") final Long applicationId,
                                         @RequestBody final List<CommandRule> rules) {
        ruleProcessingService.addCommandRuleList(userId, applicationId, rules);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/internal/{applicationId}")
    public ResponseEntity getRules(@PathVariable(name = "applicationId") final Long applicationId) {
        return ResponseEntity.ok(ruleProcessingService.getApplicationRules(applicationId));
    }

    @DeleteMapping("/internal/{applicationId}")
    public ResponseEntity deleteRules(@PathVariable(name = "applicationId") final Long applicationId) {
        ruleProcessingService.deleteCommandRules(applicationId);
        return ResponseEntity.ok().build();
    }
}
