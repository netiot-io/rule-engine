package io.netiot.ruleengine.configuration;

import io.netiot.ruleengine.models.CommandData;
import io.netiot.ruleengine.models.EventData;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;

import static io.netiot.ruleengine.configuration.KafkaBindings.DATA_IN;
import static io.netiot.ruleengine.configuration.KafkaBindings.DATA_OUT;

public interface KafkaChannels {
    @Input(DATA_IN)
    KStream<String, EventData> dataInput();

    @Output(DATA_OUT)
    KStream<String, CommandData> dataOutput();
}
