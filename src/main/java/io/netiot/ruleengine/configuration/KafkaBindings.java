package io.netiot.ruleengine.configuration;

public final class KafkaBindings {
    private KafkaBindings() {}

    public static final String DATA_IN = "data-input";
    public static final String DATA_OUT = "data-output";
}
