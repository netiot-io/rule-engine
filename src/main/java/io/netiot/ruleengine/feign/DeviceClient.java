package io.netiot.ruleengine.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "devices")
public interface DeviceClient {
    @GetMapping("/V1/devices/internal/{deviceId}/topic")
    String getDeviceTopic(@PathVariable(name = "deviceId") final Long deviceId);

    @GetMapping("/V1/devices/internal/{deviceId}")
    String getDeviceName(@PathVariable(name = "deviceId") final Long deviceId);
}
